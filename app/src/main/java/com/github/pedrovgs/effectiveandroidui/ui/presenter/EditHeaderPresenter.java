package com.github.pedrovgs.effectiveandroidui.ui.presenter;

import javax.inject.Inject;

/**
 * Created by binhtv on 4/23/2015.
 */
public class EditHeaderPresenter extends Presenter
{
    @Inject
    public EditHeaderPresenter()
    {

    }

    @Override
    public void initialize()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }

    public interface View
    {
        void showHeader(String header);
    }
}
