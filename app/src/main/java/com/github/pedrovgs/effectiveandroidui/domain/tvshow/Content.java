package com.github.pedrovgs.effectiveandroidui.domain.tvshow;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by binhtv on 4/23/2015.
 */
public class Content implements Parcelable
{
    public static final String EXTRA_CONTENT = "extra_content";
    public static final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>()
    {
        public Content createFromParcel(Parcel source)
        {
            return new Content(source);
        }

        public Content[] newArray(int size)
        {
            return new Content[size];
        }
    };
    private String content;

    public Content(String content)
    {
        this.content = content;
    }

    private Content(Parcel in)
    {
        this.content = in.readString();
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.content);
    }

}
