package com.github.pedrovgs.effectiveandroidui.ui.fragment;

import android.preference.EditTextPreference;
import com.github.pedrovgs.effectiveandroidui.R;
import com.github.pedrovgs.effectiveandroidui.ui.presenter.EditHeaderPresenter;

import javax.inject.Inject;

/**
 * Created by binhtv on 4/23/2015.
 */
public class EditHeaderFragment extends BaseFragment implements EditHeaderPresenter.View
{

    @Inject
    EditHeaderPresenter editHeaderPresenter;

    public static EditHeaderFragment newInstance()
    {
        return new EditHeaderFragment();
    }

    @Override
    protected int getFragmentLayout()
    {
        return R.layout.fragment_edit_header;
    }

    @Override
    public void showHeader(String header)
    {

    }
}
