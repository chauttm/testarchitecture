package com.github.pedrovgs.effectiveandroidui.ui.presenter;

import android.util.Log;
import com.github.pedrovgs.effectiveandroidui.domain.tvshow.Content;

import javax.inject.Inject;

/**
 * Created by binhtv on 4/22/2015.
 */
public class TvTestPresenter extends Presenter
{
    private View view;
    private Content content;

    @Inject
    public TvTestPresenter()
    {

    }

    public View getView()
    {
        return view;
    }

    public void setView(View view)
    {
        this.view = view;
    }

    public Content getContent()
    {
        return content;
    }

    public void setContent(Content content)
    {
        this.content = content;
    }

    @Override
    public void initialize()
    {
        checkViewAlreadySetted();
        loadContent();
    }

    private void loadContent()
    {
        if (view.isReady())
        {
            view.showLoading();
        }
        showContent(content.getContent());
    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }

    private void showContent(String content)
    {
        if (view.isReady())
        {
            view.hideLoading();
            view.showTvMessage(content);
        }
    }

    private void checkViewAlreadySetted()
    {
        if (view == null)
        {
            throw new IllegalArgumentException("Remember to set a view for this presenter");
        }
    }

    public interface View
    {
        void showLoading();

        void hideLoading();

        boolean isReady();

        boolean isAlreadyLoaded();

        void showTvMessage(String content);
    }
}
