package com.github.pedrovgs.effectiveandroidui.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.InjectView;
import com.github.pedrovgs.effectiveandroidui.R;
import com.github.pedrovgs.effectiveandroidui.domain.tvshow.Content;
import com.github.pedrovgs.effectiveandroidui.ui.presenter.TvTestPresenter;

import javax.inject.Inject;

/**
 * Created by binhtv on 4/22/2015.
 */
public class TvTestFragment extends BaseFragment implements TvTestPresenter.View
{
    @InjectView(R.id.fragment_tv_test_tvContent)
    TextView tvContent;
    @InjectView(R.id.pb_loading)
    ProgressBar pgLoading;
    @Inject
    TvTestPresenter presenter;

    public static Fragment newInstance(Content content)
    {
        Bundle args = new Bundle();
        args.putParcelable(Content.EXTRA_CONTENT, content);
        TvTestFragment tvTestFragment = new TvTestFragment();
        tvTestFragment.setArguments(args);
        return tvTestFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null)
        {
            setData(getArguments());
        }
        presenter.setView(this);
        presenter.initialize();
    }

    private void setData(Bundle arguments)
    {
        Content content = arguments.getParcelable(Content.EXTRA_CONTENT);
        presenter.setContent(content);
    }

    @Override
    protected int getFragmentLayout()
    {
        return R.layout.fragment_tv_test;
    }

    @Override
    public void showLoading()
    {
        pgLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading()
    {
        pgLoading.setVisibility(View.GONE);
    }

    @Override
    public boolean isReady()
    {
        return isAdded();
    }

    @Override
    public boolean isAlreadyLoaded()
    {
        return false;
    }

    @Override
    public void showTvMessage(String content)
    {
        tvContent.setText(content);
    }

}
